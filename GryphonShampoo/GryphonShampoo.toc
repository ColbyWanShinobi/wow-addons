## Interface: 60200
## Title: GryphonShampoo
## Notes: Hides the Gryphons on either side of the default action bars
## Author: ColbyWanShinobi
## Version: 1.0
## X-Category: Interface Enhancements

GryphonShampoo.lua
