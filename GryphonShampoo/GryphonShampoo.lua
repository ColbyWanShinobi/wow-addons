--[[
GryphonShampoo
Created by ColbyWanShinobi
email: colbywanshinobi@gameshaman.com
web: gameshaman.com
repo: https://ColbyWanShinobi@bitbucket.org/ColbyWanShinobi/wow-addons.git
--]]

--Print a status message upon load
print("Gryphon Shampoo Loaded. Gryphons have been removed!")

--Hide the Left Gryphon
MainMenuBarLeftEndCap:Hide()

--Hide the Right Gryphon
MainMenuBarRightEndCap:Hide()
